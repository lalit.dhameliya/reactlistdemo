import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from './Home';
import FlatListBasics from './FlatListBasics';
import SectionListBasics from './SectionListBasics';

const MainNavigator = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            headerTitle: "Home",
            headerStyle: {
                backgroundColor: '#64b5f6'
            },
            headerTitleStyle: {
                color: '#ffffff',
                fontWeight: 'bold'
            }
        },
    },
    FlatListBasics: {
        screen: FlatListBasics,
        navigationOptions: {
            headerTitle: "Flat List",
            headerStyle: {
                backgroundColor: '#64b5f6'
            },
            headerTitleStyle: {
                color: '#ffffff',
                fontWeight: 'bold'
            },
            headerTintColor: "white"
        },
    },
    SectionListBasics: {
        screen: SectionListBasics,
        navigationOptions: {
            headerTitle: "Section List",
            headerStyle: {
                backgroundColor: '#64b5f6'
            },
            headerTitleStyle: {
                color: '#ffffff',
                fontWeight: 'bold'
            },
            headerTintColor: "white"
        },
    },
})

const AppNavigator = createAppContainer(MainNavigator);

export default AppNavigator;