import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';

export default class Home extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate("FlatListBasics")
                }}
                    style={styles.touchableButton}
                >
                    <Text style={styles.item}>Flat List</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate("SectionListBasics")
                }}
                    style={styles.touchableButton}
                >
                    <Text style={styles.item}>Section List</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        padding: 24
    },
    touchableButton: {
        backgroundColor: "#0080FF",
        margin: 10
    },
    item: {
        fontSize: 18,
        color: "white",
        alignSelf: "center",
        padding: 10
    },
})